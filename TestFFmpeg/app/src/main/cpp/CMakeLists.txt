# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

#设置include头文件的路径
include_directories(
        ${CMAKE_SOURCE_DIR}/include
)

#设置lib_src_DIR指向jniLibs的对应架构目录
set(lib_src_DIR ${CMAKE_SOURCE_DIR}/../jniLibs/${ANDROID_ABI})

#导入libavcodec.so,库名为avcodec-lib
add_library(avcodec-lib SHARED IMPORTED)
set_target_properties(avcodec-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libavcodec.so)

#导入libavfilter.so,库名为avfilter-lib
add_library(avfilter-lib SHARED IMPORTED)
set_target_properties(avfilter-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libavfilter.so)

#导入libavformat.so,库名为avformat-lib
add_library(avformat-lib SHARED IMPORTED)
set_target_properties(avformat-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libavformat.so)

#导入libavutil.so,库名为avutil-lib
add_library(avutil-lib SHARED IMPORTED)
set_target_properties(avutil-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libavutil.so)

#导入libpostproc.so,库名为postproc-lib
add_library(postproc-lib SHARED IMPORTED)
set_target_properties(postproc-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libpostproc.so)

#导入libswresample.so,库名为swresample-lib
add_library(swresample-lib SHARED IMPORTED)
set_target_properties(swresample-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libswresample.so)

#导入libswscale.so,库名为swscale-lib
add_library(swscale-lib SHARED IMPORTED)
set_target_properties(swscale-lib PROPERTIES IMPORTED_LOCATION
        ${lib_src_DIR}/libswscale.so)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
             native-lib

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             native-lib.cpp )

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
                       native-lib

                       # Links the target library to the log library
                       # included in the NDK.
                        -landroid
                       ${log-lib}
                        avcodec-lib
                        avfilter-lib
                        avformat-lib
                        avutil-lib
                        postproc-lib
                        swresample-lib
                        swscale-lib)
# 1.环境

- Windows10的linux子系统

- FFmpeg-n3.4.7

- x264-master

- android-ndk-r14b

# 2.如果windows10还没使用linux子系统，快来跟着我教程来试试

- 设置开发者人员模式

  <img src=".\ffmpeg01.png" alt="ffmpeg01" style="zoom:50%;" />

  <img src=".\ffmpeg02.png" alt="ffmpeg02" style="zoom: 25%;" />

  <img src=".\ffmpeg03.png" alt="ffmpeg03" style="zoom:50%;" />

- 在windows功能那里启用适用于windows的linux子系统

  <img src=".\ffmpeg04.png" alt="ffmpeg04" style="zoom: 25%;" />

  <img src=".\ffmpeg05.png" alt="ffmpeg05" style="zoom: 25%;" />

  <img src="ffmpeg06.png" alt="ffmpeg06" style="zoom:50%;" />

- 到microsoft store下载安装ubuntu（可能有显示你可以在 Xbox One 主机上购买。(你所在的地区不支持通过 microsoft.com 购买。),但是没关系，继续点击获取进行下载。）。下载完成后右下角弹窗提示，选择启动，然后输入用户名和密码来初始化

  <img src="ffmpeg07.png" alt="ffmpeg07" style="zoom: 33%;" />

- 启动windows命令行（比如win+r，输入cmd确定），输入bash回车，进入linux

  ![ffmpeg08](ffmpeg08.png)

- 怎么安装cmake。第一次使用ubuntu，要先用sudo apt-get update来更新软件源库。更新完后使用sudo apt-get install cmake来安装。如果提示[y/NO]时，输入y继续
- 怎么切换到其它盘目录。看命令行可以在知道c盘挂在mnt目录下，比如要切换到d:/test目录，输入 cd /mnt/d/test就可以了

# 3.下载ffmpeg源代码和ndk

- 从官网或其它地方下载ffmpeg代码。我是从github上下载。听说4的版本不支持mediacodec硬解码，所以我决定下载3.4.7版本

  ![ffmpeg09](ffmpeg09.png)

- 从官网或其它地方下载ndk。我是从官网https://developer.android.google.cn/ndk/downloads/older_releases.html下载。好像如果用高版本ndk编译的库，在低版本ndk中无法使用。所以我选择下载android-ndk-r14b-linux-x86_64.zip

  <img src="ffmpeg10.png" alt="ffmpeg10" style="zoom:50%;" />

- 可以进入FFmpeg的解压目录输入命令可以看ffmpeg支持情况
  查看所有编译配置选项：`./configure --help`
  查看支持的解码器：`./configure --list-decoders`
  查看支持的编码器：`./configure --list-encoders`
  查看支持的硬件加速：`./configure --list-hwaccels`

  <img src="ffmpeg11.png" alt="ffmpeg11" style="zoom: 50%;" />

- 如果需要使用ffmpeg来进行x264编码，还需要下载x264源码进行编译。https://code.videolan.org/videolan/x264可以下载。有stable和master两种下载。我决定下载master来试试新版x264

  <img src="ffmpeg12.png" alt="ffmpeg12" style="zoom: 33%;" />

# 4.编译FFMPEG库

- 解压下载的文件

  <img src="ffmpeg13.png" alt="ffmpeg13" style="zoom:50%;" />

- 进入x264的目录，编写build.sh。注意（NDK=写NDK解压的目录，最好是在linux的vi下写，否则可能会出现代码格式出错。有个小技巧，在windows下写好保存，在linux命令行上输入sed -e 's/^M/\n/g' build.sh和sed -i 's/\r$//' build.sh命令去掉多余符号）。本来我想把android-21改成android-17，但是发现只有21以后才有arm64

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r14b
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      HOST=""
      CROSS_PREFIX=""
      SYSROOT=""
      if [ "$CPU" == "armv7-a" ]
      then
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
      else
          HOST=aarch64-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm64/
          CROSS_PREFIX=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
      fi
      ./configure \
      --prefix=$PREFIX \
      --host=$HOST \
      --enable-pic \
      --enable-static \
      --enable-neon \
      --extra-cflags="-fPIE -pie" \
      --extra-ldflags="-fPIE -pie" \
      --cross-prefix=$CROSS_PREFIX \
      --sysroot=$SYSROOT
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
  
      configure $cpu
      #-j<CPU核心数>
      make -j4
      make install
  }
  
  build arm64
  build armv7-a
  ```

- 进入x264目录，输入./build.sh运行脚本。运行完成后，可以看到目录下有android文件夹，里面有arm64和armv7-a目录，都有.a库和include头文件

  ![ffmpeg14](ffmpeg14.png)

  ![ffmpeg15](ffmpeg15.png)

  ![ffmpeg16](ffmpeg16.png)

- 关于修改ffmpeg的configure。网上说要修改，我没有修改，发现编译出来的文件没有问题。

  ![ffmpeg17](ffmpeg17.png)

- 进入FFmpeg的目录，编写build.sh。注意（NDK=写NDK解压的目录，x264=写x264目录。最好是在linux的vi下写，否则可能会出现代码格式出错。有个小技巧，在windows下写好保存，在linux命令行上输入sed -e 's/^M/\n/g' build.sh和sed -i 's/\r$//' build.sh命令去掉多余符号）

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r14b
  ADDI_CFLAGS="-fPIE -pie"
  ADDI_LDFLAGS="-fPIE -pie"
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      x264=$(pwd)/../x264-master/android/$CPU
      HOST=""
      CROSS_PREFIX=""
      SYSROOT=""
      ARCH=""
      if [ "$CPU" == "armv7-a" ]
      then
          ARCH="arm"
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
      else
          ARCH="aarch64"
          HOST=aarch64-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm64/
          CROSS_PREFIX=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
      fi
      ./configure \
      --prefix=$PREFIX \
      --disable-encoders \
      --disable-decoders \
      --disable-avdevice \
      --disable-static \
      --disable-doc \
      --disable-ffplay \
      --disable-network \
      --disable-doc \
      --disable-symver \
      --enable-neon \
      --enable-shared \
      --enable-libx264 \
      --enable-gpl \
      --enable-pic \
      --enable-jni \
      --enable-pthreads \
      --enable-mediacodec \
      --enable-encoder=aac \
      --enable-encoder=gif \
      --enable-encoder=libopenjpeg \
      --enable-encoder=libmp3lame \
      --enable-encoder=libwavpack \
      --enable-encoder=libx264 \
      --enable-encoder=mpeg4 \
      --enable-encoder=pcm_s16le \
      --enable-encoder=png \
      --enable-encoder=srt \
      --enable-encoder=subrip \
      --enable-encoder=yuv4 \
      --enable-encoder=text \
      --enable-decoder=aac \
      --enable-decoder=aac_latm \
      --enable-decoder=libopenjpeg \
      --enable-decoder=mp3 \
      --enable-decoder=mpeg4_mediacodec \
      --enable-decoder=pcm_s16le \
      --enable-decoder=flac \
      --enable-decoder=flv \
      --enable-decoder=gif \
      --enable-decoder=png \
      --enable-decoder=srt \
      --enable-decoder=xsub \
      --enable-decoder=yuv4 \
      --enable-decoder=vp8_mediacodec \
      --enable-decoder=h264_mediacodec \
      --enable-decoder=hevc_mediacodec \
      --enable-hwaccel=h264_mediacodec \
      --enable-hwaccel=mpeg4_mediacodec \
      --enable-ffmpeg \
      --enable-bsf=aac_adtstoasc \
      --enable-bsf=h264_mp4toannexb \
      --enable-bsf=hevc_mp4toannexb \
      --enable-bsf=mpeg4_unpack_bframes \
      --enable-cross-compile \
      --cross-prefix=$CROSS_PREFIX \
      --target-os=android \
      --arch=$ARCH \
      --sysroot=$SYSROOT \
      --extra-cflags="-I$x264/include $ADDI_CFLAGS" \
      --extra-ldflags="-L$x264/lib"
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
      
      configure $cpu
      make -j4
      make install
  }
  
  build arm64
  build armv7-a
  ```

- 进入ffmpeg目录，输入./build.sh运行脚本。运行完成后，可以看到目录下有android文件夹，里面有arm64和armv7-a目录，都有.a库和include头文件

  ![ffmpeg18](ffmpeg18.png)

  ![ffmpeg19](ffmpeg19.png)

# 5.使用ffmpeg库

- 新建工程进行测试使用。好像android studio支持cmake，而且推荐使用。所以这次我打算用cmake来测试。（旧版android studio是勾选**Include C++ Support**，新版android studio是选择native C++）

  <img src="ffmpeg20.png" alt="ffmpeg20" style="zoom:50%;" />

- 建完工程，可以看cmake的几个关键文件和目录

  <img src="ffmpeg21.png" alt="ffmpeg21" style="zoom:50%;" />

  <img src="ffmpeg22.png" alt="ffmpeg22" style="zoom:50%;" />

- 对工程进行简单设置，把编译架构改成只支持armeabi-v7a和arm64-v8a，然后编译运行确定工程没有问题。（这里我有遇到问题，因为我原来是把工程放到android project文件夹中，结果编译时clean命令报错。网上查了下，把android project的中间空格去掉，重新打开工程编译没有问题了。原本想设置生成的so动态库最后输出的路径，但是发现有问题，这后面会提一下）

  ```
  externalNativeBuild {
      cmake {
          cppFlags ""
          abiFilters "armeabi-v7a" , "arm64-v8a"
      }
  }
  
  ```

  <img src="ffmpeg23.png" alt="ffmpeg23" style="zoom:50%;" />

- 注意如果只点三角形按钮直接运行到手机上，可能只编译了一个架构的库，比如arm64-v8a，在build选项那里选择make Module ‘app’，则可以看到app\build\intermediates\transforms\mergeJniLibs\debug\0\lib那里有多个架构的库

  <img src="ffmpeg24.png" alt="ffmpeg24" style="zoom:50%;" />

- 成功运行会在手机上看到Hello from C++（第一次运行失败，显示有个文件被占用，重新运行成功）

  <img src="ffmpeg25.png" alt="ffmpeg25" style="zoom:33%;" />

- 现在开始加点小细节，把ffmpeg库加进去编译运行。把ffmpeg目录下的库拷贝到android工程的jniLibs对应目录下。把android/armv7-a/lib的so文件拷贝到jniLibs/armeabi-v7a，把android/arm64/lib的so文件拷贝到jniLibs/arm64-v8a。（如果app/src/main没有jniLibs目录，新新建一个，jniLibs目录下如果没有armeabi-v7a和arm64-v8a目录也新建一个）

  <img src="ffmpeg26.png" alt="ffmpeg26" style="zoom:50%;" />

- 把ffmpeg目录下把头文件拷贝到工程中。在android/arm64或android/armv7-a目录下把include文件夹拷贝到工程的cpp目录下

  <img src="ffmpeg27.png" alt="ffmpeg27" style="zoom:50%;" />

- 在cmake的文件上写上头文件路径和ffmpeg的so库路径，${CMAKE_SOURCE_DIR}为CMakeLists.text所在的目录，注意相对路径。

  ```
  #设置include头文件的路径
  include_directories(
          ${CMAKE_SOURCE_DIR}/include
  )
  
  #设置lib_src_DIR指向jniLibs的对应架构目录
  set(lib_src_DIR ${CMAKE_SOURCE_DIR}/../jniLibs/${ANDROID_ABI})
  
  #导入libavcodec.so,库名为avcodec-lib
  add_library(avcodec-lib SHARED IMPORTED)
  set_target_properties(avcodec-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavcodec.so)
  
  #导入libavfilter.so,库名为avfilter-lib
  add_library(avfilter-lib SHARED IMPORTED)
  set_target_properties(avfilter-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavfilter.so)
  
  #导入libavformat.so,库名为avformat-lib
  add_library(avformat-lib SHARED IMPORTED)
  set_target_properties(avformat-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavformat.so)
  
  #导入libavutil.so,库名为avutil-lib
  add_library(avutil-lib SHARED IMPORTED)
  set_target_properties(avutil-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavutil.so)
  
  #导入libpostproc.so,库名为postproc-lib
  add_library(postproc-lib SHARED IMPORTED)
  set_target_properties(postproc-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libpostproc.so)
  
  #导入libswresample.so,库名为swresample-lib
  add_library(swresample-lib SHARED IMPORTED)
  set_target_properties(swresample-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libswresample.so)
  
  #导入libswscale.so,库名为swscale-lib
  add_library(swscale-lib SHARED IMPORTED)
  set_target_properties(swscale-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libswscale.so)
  ```

  ```
  target_link_libraries( # Specifies the target library.
                         native-lib
  
                         # Links the target library to the log library
                         # included in the NDK.
                         ${log-lib}
                          avcodec-lib
                          avfilter-lib
                          avformat-lib
                          avutil-lib
                          postproc-lib
                          swresample-lib
                          swscale-lib)
  ```

- 在native-lib.c文件调用一下ffmpeg的方法，如果编译运行后没报错，那就成功了

  <img src="ffmpeg28.png" alt="ffmpeg28" style="zoom:50%;" />

- 关于设置库输出路径，我遇到两个问题，其中一个问题找不到解决方法。第一个问题，设置输出目录到jniLibs下了，编译运行时报错有多个库存在，因为so库在libs目录下和jniLibs目录下的冲突，所以要设置sourceSets指定lib目录只有jniLibs。第二个问题，引用ffmpeg库时，编译运行报错找不到libavcodec.so文件，找不到解决方法

  <img src="ffmpeg29.png" alt="ffmpeg29" style="zoom:50%;" />

  <img src="ffmpeg30.png" alt="ffmpeg30" style="zoom:50%;" />

# 6.解码视频并播放

- 继续在第5步的ffmpeg工程上继续开发。首先在native-lib.c文件上写好解码代码，然后在java上调用测试没问题，最后用surface来显示解码得到的画面。第一步，肯定要把log功能弄好，调试才方便

- FFmpeg提供av_log_set_callback的方法给我们，设置后可以获取到FFmpeg的打印信息。我们把ffmpeg的打印信息输出到android的logcat上

  ```
  #include <android/log.h>
  extern "C" {
  #include <libavformat/avformat.h>
  #include <libavcodec/avcodec.h>
  #include <libavutil/opt.h>
  #include <libavutil/imgutils.h>
  #include <libswscale/swscale.h>
  #include "libavcodec/jni.h"
  }
  const char* TAG = "native-lib,leeson,log";
  
  #define ALOGE(fmt, ...) __android_log_vprint(ANDROID_LOG_ERROR, TAG, fmt, ##__VA_ARGS__)
  #define ALOGI(fmt, ...) __android_log_vprint(ANDROID_LOG_INFO, TAG, fmt, ##__VA_ARGS__)
  #define ALOGD(fmt, ...) __android_log_vprint(ANDROID_LOG_DEBUG, TAG, fmt, ##__VA_ARGS__)
  #define ALOGW(fmt, ...) __android_log_vprint(ANDROID_LOG_WARN, TAG, fmt, ##__VA_ARGS__)
  #define ALOGV(fmt, ...) __android_log_vprint(ANDROID_LOG_VERBOSE, TAG, fmt, ##__VA_ARGS__)
  
  jboolean FFMPEG_ANDROID_DEBUG = 0;
  
  static void ffmpeg_android_log_callback(void *ptr, int level, const char *fmt, va_list vl){
      if (FFMPEG_ANDROID_DEBUG){
          switch(level) {
              case AV_LOG_DEBUG:
                  ALOGD(fmt, vl);
                  break;
              case AV_LOG_VERBOSE:
                  ALOGV(fmt, vl);
                  break;
              case AV_LOG_INFO:
                  ALOGI(fmt, vl);
                  break;
              case AV_LOG_WARNING:
                  ALOGW(fmt, vl);
                  break;
              case AV_LOG_ERROR:
                  ALOGE(fmt, vl);
                  break;
          }
      }
  }
  
  ```

- 调用其它ffmpeg方法前先运行下面两句代码就可以在android logcat看到ffmpeg的日志信息了。

  ```
  FFMPEG_ANDROID_DEBUG = 1;
  av_log_set_callback(ffmpeg_android_log_callback);
  ```

- 要实现一个方法，叫JNI_OnLoad,因为ffmpeg使用mediacodec硬解码时需要JVM

  ```
  extern "C"
  JNIEXPORT
  jint JNI_OnLoad(JavaVM *vm, void *res) {
      av_jni_set_java_vm(vm, res);
      // 返回jni版本
      return JNI_VERSION_1_6;
  }
  ```

- 解码播放使用surface，jni端需要用到ANativeWindow，因此cmake要添加-landroid库

  ```
  target_link_libraries( # Specifies the target library.
                         native-lib
  
                         # Links the target library to the log library
                         # included in the NDK.
                          -landroid
  ```

  

- 按照已有的方法，重新建个方法，函数命名方式是包名+类名+方法名，我这里叫playWithSurface，把视频地址通过String类型参数传进来,把Surface对象也传进来，使用GetStringUTFChars方法把string转换成char*使用

  ```
  int decodeVideo(const char * videoPath,JNIEnv *env);
  ANativeWindow *nativeWindow;
  extern "C" void
  Java_com_octant_testffmpeg_MainActivity_playWithSurface(
          JNIEnv *env,
          jobject,
          jstring videopath,
          jobject surface) {
  
      FFMPEG_ANDROID_DEBUG = 1;
      av_log_set_callback(ffmpeg_android_log_callback);
      const char *videoPath = env->GetStringUTFChars(videopath, JNI_FALSE);
      nativeWindow = ANativeWindow_fromSurface(env,surface);
      decodeVideo(videoPath,env);
  }
  ```

- 我们定义了decodeVide函数，开始解码视频。

  AVPacket存放读取视频的一个数据包

  AVFrame存放解码出来的一帧画面数据

  av_register_all以前是注册解码器之类的组件。不过听说现在没用了。

  ```
  int decodeVideo(const char * videoPath,JNIEnv *env){
      int ret;
      AVPacket packet;
      AVFrame *frame = av_frame_alloc();
      if (!frame ) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return -1;
      }
      av_register_all();
  ```

- 调用open_input_file来打开视频文件，并初始化一些变量。这个函数是我自己定义的

  ```
  if ((ret = open_input_file(videoPath)) < 0)
          goto end;
  ```

- 我们先转到open_input_file函数的定义。有3个变量是decodeVideo函数要用的，所以定义成全局变量。

  AVFormatContext保存视频的编码方式的上下文数据结构，比如avc、divx

  AVCodecContext保存的是要使用的编解码器上下文的数据结构

  Video_stream_index保存画面index，因为视频有声音和画面，读取的数据包根据index判断是哪种类型数据

  AVCodec保存要用的解码器

  avformat_open_input打开视频文件，提取视频编码信息到fmt_ctx变量

  avformat_find_stream_info根据传入的视频编码信息判断是否能找到流

  ```
  AVFormatContext *fmt_ctx;
  AVCodecContext *dec_ctx;
  int video_stream_index = -1;
  int open_input_file(const char *filename)
  {
      int ret;
      AVCodec *dec;
  
      if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
          return ret;
      }
  
      if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
          return ret;
      }
  ```

- av_find_best_stream根据传入的视频编码信息得到视频流index，保存到video_stream_index。

  得到解码器，保存到dec。网上很多人是用avcodec_find_decoder(id)l来寻找解码器或用avcodec_find_decoder_by_name("h264_mediacodec")寻找指定解码器，因为h264_mediacodec解码器和h264解码器id相同。我发现这个方法找到的解码器是硬解码器h264_mediacodec

  ```
  /* select the video stream */
      ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
      if (ret < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
          return ret;
      }
      video_stream_index = ret;
  ```

- 创建并初始化解码器上下文的数据结构，保存到dec_ctx

  ```
      /* create decoding context */
      dec_ctx = avcodec_alloc_context3(dec);
      if (!dec_ctx)
          return AVERROR(ENOMEM);
  
      avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
      av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);
  
      /* init the video decoder */
      if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return ret;
      }
  ```

- open_input_file函数内容已经讲完，现在转回到decode_video函数。现在开始进入读取视频并解码流程。

  av_read_frame读取一个数据包

  判断packet的index是不是video stream的

  avcodec_send_packet把packet发给解码器上下文结构体进行解码

  avcodec_receive_frame接收解码器上下文件结构体解码出来的avframe数据

  ```
      while (1) {
          if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
              break;
  
          if (packet.stream_index == video_stream_index) {
              ret = avcodec_send_packet(dec_ctx, &packet);
              if (ret < 0) {
                  av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                  break;
              }
  
              while (ret >= 0) {
                  ret = avcodec_receive_frame(dec_ctx, frame);
                  if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                      break;
                  } else if (ret < 0) {
                      av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                      goto end;
                  }
  ```

- 得到解码的数据后，直接打印一句，说明程序能正确跑到这里。android surface显示是rgba格式，通过ANativeWindow_lock和ANativeWindow_unlockAndPost刷新surface的画面。通过sws_scale可以转换数据格式，也可以通过opencv或网上算法根据avframe的format值进行转换。最后把一些变量释放内存。

  ```
                  if (ret >= 0) {
                      av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                      
                      sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0, dec_ctx->height, pRgbFrame->data, pRgbFrame->linesize);
  
                      ANativeWindow_lock(nativeWindow, &windowBuffer, 0);
  
                      memcpy(dst_data,pRgbBuff,dec_ctx->width*dec_ctx->height*4);
                      ANativeWindow_unlockAndPost(nativeWindow);
  
  
                      av_frame_unref(frame);
                  }
              }
          }
          av_packet_unref(&packet);
      }
      end:
      avcodec_free_context(&dec_ctx);
      avformat_close_input(&fmt_ctx);
      av_frame_free(&frame);
      return 0;
  ```

- Jni部分写完了，我们开始调用。首先弄好权限先，在AndroidManifest.xml添加权限。

  ```
  <!--写入扩展存储，向扩展卡写入数据-->
      <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"></uses-permission>
  ```

- Android6.0以上版本需要动态申请权限

  ```
          if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
              int writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
  
              if (writePermission != PackageManager.PERMISSION_GRANTED) {
                  // We don't have permission so prompt the user
                  String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
  
                  int permsRequestCode = 200;
  
                  requestPermissions(perms, permsRequestCode);
              }
          }
  ```

  ```
      public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
  
          switch (permsRequestCode) {
  
              case 200:
  
                  boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                  if (false == writeAccepted) {
                      if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                          String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
  
                          requestPermissions(perms, permsRequestCode);
                      }
                  }
  
                  break;
          }
      }
  ```

- 权限弄好后，就开始在java上调用jni函数了。在xml加个surface控件，在activity上初始化surface并传输给jni。权限申请成功和surface created后才启动解码，否则播放视频失败。注意，根据代码，视频要记得放在对应路径上，比如把MOVI0001.mov放到手机根目录。

  ```
  <SurfaceView
          android:id="@+id/surfaceView"
          android:layout_width="match_parent"
          android:layout_height="match_parent"
          android:layout_centerInParent="true"/>
  ```

  ```
  public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback
  ```

  ```
  SurfaceView surfaceView;
  SurfaceHolder surfaceHolder;
  ```

  ```
  surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
  surfaceHolder = surfaceView.getHolder();
  
  surfaceHolder.addCallback(this);
  ```

  ```
  public native void playWithSurface(String videopath, Object surface);	
  ```

  ```
  @Override
  public void surfaceCreated(SurfaceHolder surfaceHolder) {
  surfaceCreatedFlag = true;
  }
  ```

  ```
  	Thread playVideoThread = new Thread(new Runnable() {
          @Override
          public void run() {
              while (surfaceCreatedFlag == false || writePermittedFlag == false){
                  try {
                      Thread.sleep(100);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
              }
              String path = Environment.getExternalStorageDirectory().getAbsolutePath();
              File dir = new File(path);
              if (!dir.exists()) {
                  dir.mkdirs();
              }
              final String inputPath = path + "/MOVI0001.mov";
              playWithSurface(inputPath,surfaceHolder.getSurface());
          }
      });
  ```

  ```
  playVideoThread.start();
  ```

  